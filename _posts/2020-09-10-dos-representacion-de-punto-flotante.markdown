---
layout: post
title:  "Introducción"
date:   2020-09-10 18:51:43 -0400
categories: rpf
tag: 0
---

# Representación de punto flotante

![Representacion de Punto Flotante](/assets/images/rpf.png)

  Muchas aplicaciones requieren trabajar con números que no son enteros, es por ello que existen varias formas de representar números no enteros, comúnmente se usa la que se conoce como representación en punto flotante. Bajo este esquema, un número puede ser expresado mediante un exponente y una mantisa.
  ¿Qué se necesita para representar un número en punto flotante?
  - El signo del número.
  - El signo del exponente.
  - Dígitos para el exponente.
  - Dígitos para la mantisa.
