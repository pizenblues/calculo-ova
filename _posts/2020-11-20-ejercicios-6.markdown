---
layout: post
title:  "Ejercicios propuestos"
date:   2020-11-20 18:51:43 -0400
categories: edo
tag: 11
---

# Ejercicios Propuestos

<br>
<iframe width="560" height="315" src="https://www.youtube.com/embed/yFEf4VBIFCU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

1- Dada la ecuación diferencial {% katex %} Y'=yx-1{% endkatex %}, en el intervalo [0,2] con {% katex %}y(0)=1{% endkatex %} como condición inicial, aplicar Euler con {% katex %} h=0,5{% endkatex %} y {% katex %} h=0,25{% endkatex %}

La siguiente fórmula:
{% katex %} f'(x_i)= \frac{f(x_i)-0}{x_i-X_{i+1}} {% endkatex %}

2- Dado el problema de valor inicial {% katex %} Y'=-y+x+1 {% endkatex %}, en el intervalo [0,1] con {% katex %} y(0)=1 {% endkatex %}, {% katex %} h=0,1 {% endkatex %}, aplique Runge - Kutta


3- Dada la ecuación diferencial {% katex %} Y'=yx^2-1 {% endkatex %}, en el intervalo [0,2] con {% katex %} y(0)=1 {% endkatex %} 

- Aplicar el método de Euler con {% katex %} h=0,5 {% endkatex %} y {% katex %} h=0,25 {% endkatex %}

- Runge-Kutta de orden 4 con {% katex %} h=0,5 {% endkatex %}



