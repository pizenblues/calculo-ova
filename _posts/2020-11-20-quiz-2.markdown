---
layout: post
title:  "Quiz de repaso"
date:   2020-11-20 18:51:43 -0400
categories: rpf
tag: 10
---

<h1> Quiz de repaso </h1>

<div>
    <p class="question">1. ¿En cuantos campos o bloques y en qué orden se divide la memoria?</p>
    <ul>
    <input class="answer" type="radio" name="q1" value="1">
      <label id="correctString1">Signo, Exponencial y mantisa</label>
    <br>
      <input class="answer" type="radio" name="q1" value="0">
      <label>Exponencial, mantisa y signo</label>
      <br>
      <input class="answer" type="radio" name="q1" value="0">
      <label>Signo, mantisa y exponencial</label>
      <br>
      <input class="answer" type="radio" name="q1" value="0">
      <label>Exponencial</label>
    </ul>
  </div>

  <div>
    <p class="question">2. El bit del signo puede ser positivo o negativo, ¿qué valor se toma en cuenta para calificarlo?</p>
    <ul>
      <input class="answer" type="radio" name="q2" value="0">
      <label>J=0=-,J=1=+¿</label>
      <br>
      <input class="answer" type="radio" name="q2" value="1">
      <label id="correctString2">&delta;=-¿1,&delta;=+0¿</label>
      <br>
      <input class="answer" type="radio" name="q2" value="0">
      <label>J=0=+,J=1=-¿</label>
      <br>
      <input class="answer" type="radio" name="q2" value="0">
      <label>J=x=+,x=1=-¿</label>
    </ul>
  </div>

  <div>
    <p class="question">3. ¿Cuánto es la conversión del numero <b>21<sub>10</sub></b> a binario?</p>
    <ul>
      <input class="answer" type="radio" name="q3" value="0">
      <label>111<sub>2</sub></label>
      <br>
      <input class="answer" type="radio" name="q3" value="1">
      <label id="correctString3">10101<sub>2</sub></label>
      <br>
      <input class="answer" type="radio" name="q3" value="0">
      <label>10010<sub>2</sub></label>
      <br>
      <input class="answer" type="radio" name="q3" value="0">
      <label>1001101<sub>2</sub></label>
    </ul>
</div>

<div>
  <p class="question">4. Convertir <b>0,5<sub>10</sub></b> a binario y hallar su representación con 32 bits, ¿da como resultado?</p>
  <ul>
    <input class="answer" type="radio" name="q4" value="0">
    <label>
      <table class="mathTable">
        <tr>
          <td class="mc">0</td>
          <td class="mc">1000</td>
          <td class="mc">0...0</td>
        </tr>
      </table>
    </label>
    <br>
    <input class="answer" type="radio" name="q4" value="1">
    <label id="correctString4">
      <table class="mathTable">
        <tr>
          <td class="mc">0</td>
          <td class="mc">01111110</td>
          <td class="mc">0...0</td>
        </tr>
      </table>
    </label>
    <br>
    <input class="answer" type="radio" name="q4" value="0">
    <label>
      <table class="mathTable">
        <tr>
          <td class="mc">0</td>
          <td class="mc">11101</td>
          <td class="mc">0...0</td>
        </tr>
      </table>
    </label>
    <br>
    <input class="answer" type="radio" name="q4" value="0">
    <label>
      <table class="mathTable">
        <tr>
          <td class="mc">0</td>
          <td class="mc">1110011</td>
          <td class="mc">0...0</td>
        </tr>
      </table>
    </label>
  </ul>
</div>
<!--
<div>
  <p class="question">5. ¿Qué numero decimal representa el siguiente patrón de bits?
    <table class="mathTable">
      <tr>
        <td class="mc">0</td>
        <td class="mc">1000001</td>
        <td class="mc">10100000000000000000000</td>
      </tr>
    </table>
  </p>
  <ul>
    <input class="answer" type="radio" name="q3" value="0">
    <label>131<sub>10</sub></label>
    <br>
    <input class="answer" type="radio" name="q3" value="1">
    <label>127<sub>10</sub></label>
    <br>
    <input class="answer" type="radio" name="q3" value="0">
    <label id="correctString3">26<sub>10</sub></label>
    <br>
    <input class="answer" type="radio" name="q3" value="0">
    <label>58<sub>10</sub></label>
  </ul>
</div>
-->
<br/>

<div class="submitter">
  <input class="menuButton" id="submitButton" onClick="submitQuiz()" type="submit" value="Enviar respuestas" />
</div>
<div class="quizAnswers" id="correctAnswer1"></div>
<div class="quizAnswers" id="correctAnswer2"></div>
<div class="quizAnswers" id="correctAnswer3"></div>
<div class="quizAnswers" id="correctAnswer4"></div>
<div>
  <h2 class="quizScore" id="userScore"></h2>
</div>